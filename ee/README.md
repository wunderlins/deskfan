# EE Notes

## Arduino Programming circuit:

### Programmer Circuit
[Link](https://create.arduino.cc/projecthub/arjun/programming-attiny85-with-arduino-uno-afb829),
use 10uF Cap on RST<->GND for Arduino Programmer.

### Pin Layout

![Pin Mapping ATtiny85](file:///ATtiny45-85-pins.jpg)

	ISP    <-> Attiny85
	-------------------
	5V     <-> Vcc
	Gnd    <-> Gnd
	Pin 13 <-> Pin 2
	Pin 12 <-> Pin 1
	Pin 11 <-> Pin 0
	Pin 10 <-> Reset

![Fritzing Schema](file:///Programming_ATtiny85_with_Arduino_Uno.png)



