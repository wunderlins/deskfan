EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ATtiny88 Motor Driver"
Date "2019-07-07"
Rev "1.0"
Comp "Simon Wunderlin"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4000 3600 4000 3650
Text GLabel 4600 3200 2    50   Input ~ 0
D5
Text GLabel 4600 2900 2    50   Input ~ 0
D2
Text GLabel 4600 2800 2    50   Input ~ 0
D1
Text GLabel 4600 2700 2    50   Input ~ 0
D0
Wire Wire Line
	4000 2350 4000 2400
$Comp
L Attiny85_Arduino:ATtiny25V-10SU_Arduino U1
U 1 1 5D21F3E6
P 4000 3000
F 0 "U1" V 3400 3450 50  0000 R CNN
F 1 "ATtiny25V-10SU" V 3400 3100 50  0000 R CNN
F 2 "Package_SO:SOIJ-8_5.3x5.3mm_P1.27mm" H 4000 3000 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 4000 3000 50  0001 C CNN
	1    4000 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery BT1
U 1 1 5D2648CC
P 2200 3000
F 0 "BT1" V 2300 3200 50  0000 L CNN
F 1 "Battery" V 2400 3150 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" V 2200 3060 50  0001 C CNN
F 3 "~" V 2200 3060 50  0001 C CNN
	1    2200 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 2800 2200 2350
Wire Wire Line
	2200 3200 2200 3650
Wire Wire Line
	2200 3650 2550 3650
$Comp
L Device:CP C1
U 1 1 5D22387F
P 2550 3350
F 0 "C1" H 2432 3304 50  0000 R CNN
F 1 "10uF" H 2432 3395 50  0000 R CNN
F 2 "Capacitor_SMD:C_2010_5025Metric_Pad1.52x2.65mm_HandSolder" H 2588 3200 50  0001 C CNN
F 3 "~" H 2550 3350 50  0001 C CNN
	1    2550 3350
	1    0    0    -1  
$EndComp
Connection ~ 2550 3650
$Comp
L Connector:Conn_01x06_Female J1
U 1 1 5D286E2F
P 5550 2700
F 0 "J1" H 5350 2300 50  0000 L CNN
F 1 "JST_SH_SM06B" V 5650 2300 50  0000 L CNN
F 2 "Connector_JST:JST_SH_SM06B-SRSS-TB_1x06-1MP_P1.00mm_Horizontal" H 5550 2700 50  0001 C CNN
F 3 "~" H 5550 2700 50  0001 C CNN
	1    5550 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2350 4850 2350
Wire Wire Line
	4850 2350 4850 2600
Wire Wire Line
	4850 2600 5350 2600
Wire Wire Line
	5350 2500 4950 2500
Wire Wire Line
	4950 2500 4950 3650
Wire Wire Line
	4950 3650 4000 3650
Text GLabel 5350 2700 0    50   Input ~ 0
D0
Text GLabel 5350 2800 0    50   Input ~ 0
D1
Text GLabel 5350 2900 0    50   Input ~ 0
D2
Text GLabel 5350 3000 0    50   Input ~ 0
D5
Wire Wire Line
	2550 3500 2550 3650
Text GLabel 2550 3200 1    50   Input ~ 0
D5
Connection ~ 4000 2350
Connection ~ 4000 3650
$Comp
L Device:CP C2
U 1 1 5D2992F7
P 3150 3350
F 0 "C2" H 3032 3304 50  0000 R CNN
F 1 "100nF" H 3032 3395 50  0000 R CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3188 3200 50  0001 C CNN
F 3 "~" H 3150 3350 50  0001 C CNN
	1    3150 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3200 3150 2350
Wire Wire Line
	3150 3500 3150 3650
Connection ~ 3150 2350
Wire Wire Line
	3150 2350 4000 2350
Connection ~ 3150 3650
Wire Wire Line
	3150 3650 4000 3650
Wire Wire Line
	2550 3650 3150 3650
Wire Wire Line
	2200 2350 3150 2350
Wire Notes Line
	1600 4350 1600 1400
Wire Notes Line
	1600 1400 6200 1400
Wire Notes Line
	6200 1400 6200 4350
Wire Notes Line
	6200 4350 1600 4350
Text Notes 2200 1800 0    98   ~ 0
PWM Generator ATtiny85
$Comp
L Connector:Conn_01x06_Female J2
U 1 1 5D2AF7A4
P 9050 2850
F 0 "J2" H 8950 2400 50  0000 L CNN
F 1 "ATtiny85" V 9200 2700 50  0000 L CNN
F 2 "Connector_JST:JST_SH_BM06B-SRSS-TB_1x06-1MP_P1.00mm_Vertical" H 9050 2850 50  0001 C CNN
F 3 "~" H 9050 2850 50  0001 C CNN
	1    9050 2850
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x08_Female J3
U 1 1 5D2B2763
P 9050 3900
F 0 "J3" H 8900 4400 50  0000 L CNN
F 1 "Tekko32" V 9200 3750 50  0000 L CNN
F 2 "Connector_JST:JST_SH_BM08B-SRSS-TB_1x08-1MP_P1.00mm_Vertical" H 9050 3900 50  0001 C CNN
F 3 "https://shop.holybro.com/holybro-tekko32-4in1-esc-35a_p1086.html" H 9050 3900 50  0001 C CNN
	1    9050 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	8850 3050 8850 3300
Wire Wire Line
	8850 3300 8650 3300
Wire Wire Line
	8650 3300 8650 3700
Wire Wire Line
	8950 3050 8950 3450
Wire Wire Line
	8950 3450 8850 3450
Wire Wire Line
	8850 3450 8850 3700
Wire Wire Line
	9150 3050 9150 3450
Wire Wire Line
	9150 3450 9050 3450
Wire Wire Line
	9050 3450 9050 3700
Wire Wire Line
	9150 3700 9150 3450
Connection ~ 9150 3450
Wire Wire Line
	9150 3450 9250 3450
Wire Wire Line
	9250 3450 9250 3700
Wire Wire Line
	9350 3700 9350 3450
Wire Wire Line
	9350 3450 9250 3450
Connection ~ 9250 3450
Wire Notes Line
	7600 4350 7600 1400
Wire Notes Line
	7600 1400 10400 1400
Wire Notes Line
	10400 1400 10400 4350
Wire Notes Line
	10400 4350 7600 4350
Text Notes 8350 2050 0    98   ~ 0
ATtiny <-> Tekko32\nWiring
$EndSCHEMATC
